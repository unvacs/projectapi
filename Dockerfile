FROM openjdk11-jre-slim
COPY target/*.jar /home/app/productapi.jar
ENTRYPOINT ["java", "-jar", "/home/app/productapi.jar", "com.itcompany.productapi.ProductapiApplication"]