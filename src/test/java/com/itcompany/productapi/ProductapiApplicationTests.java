package com.itcompany.productapi;

import com.itcompany.productapi.controller.ProductController;
import com.itcompany.productapi.entity.Product;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductapiApplicationTests {

	@LocalServerPort
	int serverPort;

	@Autowired
	private TestRestTemplate testRestTemplate;

	@Autowired
	private ProductController productController;

	@Test
	@DisplayName("Context available")
	void contextLoads() {
		assertThat(productController).isNotNull();
	}

	@Test
	@DisplayName("Get prodct by name")
	void getProductByName() {
		Product product = testRestTemplate.getForObject("/api/v1/product/product?name=IPhone", Product.class);
		assertThat(product).isNotNull();
		assertEquals(product.getPrice(), 7500);
 	}


}
