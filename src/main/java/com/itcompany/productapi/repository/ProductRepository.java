package com.itcompany.productapi.repository;

import com.itcompany.productapi.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {

    Product getByName(String name);

}
